package be.kdg.prog12.m6;

public class Film implements Comparable<Film> {
    private final String title;
    private final int year;

    public Film(int year, String title) {
        this.title = title;
        this.year = year;
    }

    public int compareTo(Film other) {
        return this.year - other.year;
    }

    public int getYear() {
        return year;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return title + " " + year;
    }
}
