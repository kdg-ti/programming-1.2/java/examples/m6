package be.kdg.prog12.m6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

public class TestCharacterStreams {
    private static String FILE = "FILES/film.txt";

    private static String FILE_COPY = "FILES/TEST/film_copy.txt";


    public static void main(String[] args) throws IOException {
        // Example without buffering
        String[] testArgs = {FILE, FILE_COPY};
        if (testArgs.length != 2 || (testArgs[0].compareTo(testArgs[1]) == 0)) {
            System.out.println("Usage: Copy source destination");
            System.exit(0);
        }
        System.out.println("Manually copying " + testArgs[0] + " to " + testArgs[1] + " character per character.");
        Path from = Path.of(testArgs[0]);
        Path to = Path.of(testArgs[1]);
        FileReader in = new FileReader(from.toFile());
        FileWriter out = new FileWriter(to.toFile());

        int c = in.read();
        while (c != -1) {
            out.write(c);
            c = in.read();
        }
        in.close();
        out.close();

        List<Film> myFilms = null;
        try {
            // With buffering:
            //myFilms = readFilms();

            // Without buffering (and using a Scanner):
            myFilms = readFilmsScanner();

            for (Film film : myFilms) {
                System.out.println(film);
            }
        } catch (IOException ioe) {
            // to be handled
        }

        // Writing using a PrintWriter
        // writeFilmsPrintWriter(myFilms);

        // Writing using a Formatter
        if (myFilms != null) {
            writeFilmsFormatter(myFilms);
        }
    }

    public static List<Film> readFilms() throws IOException {
        List<Film> list = new ArrayList<>();
        BufferedReader is = new BufferedReader(new FileReader(FILE));
        String line = is.readLine();
        while (line != null) {
            int year = Integer.parseInt(line.substring(0, 4));
            String title = line.substring(5);
            list.add(new Film(year, title));
            line = is.readLine();
        }
        is.close();
        return list;
    }

    public static List<Film> readFilmsScanner() throws IOException {
        List<Film> list = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(FILE));
            while (scanner.hasNext()) {
                int year = scanner.nextInt();
                String title = scanner.nextLine().trim();
                list.add(new Film(year, title));
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void writeFilmsPrintWriter(List<Film> list) {
        try {
            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(FILE_COPY)));
            for (Film film : list) {
                pw.format("%d %s%n", film.getYear(), film.getTitle());
            }
            pw.close();
        } catch (IOException e) {
            // to be handled
        }
    }

    public static void writeFilmsFormatter(List<Film> list) {
        try {
            Formatter fm = new Formatter(FILE_COPY);
            for (Film film : list) {
                fm.format("%d %s%n", film.getYear(), film.getTitle());
            }
            fm.close();
        } catch (IOException e) {
            // to be handled
        }
    }
}
