package be.kdg.prog12.m6;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Scanner;

public class DemoFiles {
    public static void main(String[] args) {
        final String BASE_DIRECTORY="FILES/TEST/";
        // Create Path objects:
        Path file1 = Path.of( BASE_DIRECTORY, "blabla.txt");
        Path file2 = Path.of(BASE_DIRECTORY,"subfolder1/test2.txt");
        Path newFolder = Path.of(BASE_DIRECTORY, "subfolder2", "subfolder3");
        Path image = Path.of(BASE_DIRECTORY,"..","angrybird.png");

        // Refer to 'angrybird_copy.png' within newFolder:
        Path imageCopy = newFolder.resolve("angrybird_copy.png");

        // Show the absolute path:
        System.out.println(file1.toAbsolutePath());

        // Check if a file exists:
        System.out.println("Does " + file1.getFileName() + " exist? " + Files.exists(file1));

        try {
            // Remove them if they exist:
            Files.deleteIfExists(file1);
            Files.deleteIfExists(file2);
            Files.deleteIfExists(imageCopy);
            Files.deleteIfExists(newFolder);

            Files.createFile(file1);  // Only now is the file created in the filesystem
            String text = "This is the first line of the text file\nThe second line\nAnd finally, the last line";
            Files.write(file1, text.getBytes()); // Write the text to file1
            // Create both the directory and the subdirectory:
            Files.createDirectories(file2.getParent());
            Files.createFile(file2);
            Files.createDirectories(newFolder);
            // Copy image to imageCopy
            Files.copy(image, imageCopy, StandardCopyOption.REPLACE_EXISTING);

            List<String> readLines = Files.readAllLines(file1, Charset.defaultCharset());
            for (String line : readLines) {  // Read line per line
                System.out.println("\t" + line);
            }
            System.out.println("Image file size: " + Files.size(image) + " bytes");

        } catch (IOException e) {
            e.printStackTrace();
        }

        Path myFile = Path.of(BASE_DIRECTORY, "blabla.txt");

        // Use a Scanner to read the first line:
        System.out.println("\nUse a Scanner to read the first line:");
        if (Files.exists(myFile)) {
            try {
                Scanner fileScanner = new Scanner(myFile);
                String text = fileScanner.nextLine();
                System.out.println(text);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        // Use a Scanner to read all lines:
        System.out.println("\nUse a Scanner to read all lines:");
        if (Files.exists(myFile)) {
            try {
                Scanner fileScanner = new Scanner(myFile);
                while (fileScanner.hasNext()) {
                    String text = fileScanner.nextLine();
                    System.out.println(text);
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        // Use a Scanner to read all lines and correctly free all resources:
        System.out.println("\nUse a Scanner to read all lines (with close):");
        if (Files.exists(myFile)) {
            Scanner fileScanner = null;
            try {
                fileScanner = new Scanner(myFile);
                while (fileScanner.hasNext()) {
                    String text = fileScanner.nextLine();
                    System.out.println(text);
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                if (fileScanner != null) {
                    fileScanner.close();
                }
            }
        }

        // Use a Scanner to read all lines and correctly free all resources using try-with-resources:
        System.out.println("\nUse a Scanner to read all lines (try-with-resources):");
        if (Files.exists(myFile)) {
            try (Scanner fileScanner = new Scanner(myFile)) {
                while (fileScanner.hasNext()) {
                    String text = fileScanner.nextLine();
                    System.out.println(text);
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
