package be.kdg.prog12.m6;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestByteStreams {
    private static final String TEXT_INPUT_FILE = "FILES/film.txt";

    private static String BINARY_FILE = "FILES/TEST/film.bin";


    public static void main(String[] args) {
        System.out.println("Reading movie data using a character stream ...");
        List<Film> myFilms = null;
        try {
            myFilms = readFilms();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(1);
        }

        System.out.println("Writing movie data using a DataOutputStream ...");
        writeFilmsDOS(myFilms);

        System.out.println("Reading movie data using a DataInputStream ...");
        List<Film> myFilmsBin;
        try {
            myFilmsBin = readFilmsDIS();
            for (Film film : myFilmsBin) {
                System.out.println(film);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void writeFilmsDOS(List<Film> list) {
        try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(BINARY_FILE)))) {
            for (Film film : list) {
                dos.writeInt(film.getYear());
                dos.writeUTF(film.getTitle());
            }
        } catch (IOException e) {
            String message = String.format("Problem writing %s", BINARY_FILE);
            System.err.println(message);
        }
    }

    private static List<Film> readFilmsDIS() throws IOException {
        List<Film> list = new ArrayList<>();
        try (DataInputStream dis = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(BINARY_FILE)))) {
            while (dis.available() > 0) {
                int year = dis.readInt();
                String title = dis.readUTF();
                list.add(new Film(year, title));
            }
        } catch (FileNotFoundException e) {
            String message = String.format("Problem reading %s", BINARY_FILE);
            System.err.println(message);
        }
        return list;
    }

    public static List<Film> readFilms() throws IOException {
        List<Film> list = new ArrayList<>();
        BufferedReader is = new BufferedReader(new FileReader(TEXT_INPUT_FILE));
        String line = is.readLine();
        while (line != null) {
            int year = Integer.parseInt(line.substring(0, 4));
            String title = line.substring(5);
            list.add(new Film(year, title));
            line = is.readLine();
        }
        is.close();
        return list;
    }
}
