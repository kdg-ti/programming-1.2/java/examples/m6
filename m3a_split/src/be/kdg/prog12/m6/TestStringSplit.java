package be.kdg.prog12.m6;

/**
 * In this example, the roster of a "Connect 4" game is represented by a String.
 * The roster has 6 rows, and 7 columns.
 * Using String's split method, the data is pulled in row per row and then
 * each row is divided into multiple individual elements or cells.
 * Each element is empty ('_'), red ('R') or yellow ('Y')
 */
public class TestStringSplit {
    public static void main(String[] args) {
        // The roster of a Connect 4 game:
        StringBuilder content = new StringBuilder();

        content.append("_,_,_,_,_,_,_").append("\n");
        content.append("_,_,_,_,_,_,_").append("\n");
        content.append("_,_,_,_,_,_,_").append("\n");
        content.append("_,_,_,_,_,_,_").append("\n");
        content.append("_,R,Y,Y,_,_,_").append("\n");
        content.append("_,Y,Y,R,R,_,_").append("\n");

        // Split line per line, separated by '\n'
        String[] lines = content.toString().split("\n");
        for (String line : lines) {
            // Split one line into elements by ','
            String[] elements = line.split(",");
            for (String element : elements) {
                System.out.printf("%s", element);
            }
            System.out.println();
        }
    }
}
