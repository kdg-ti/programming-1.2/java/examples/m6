package be.kdg.prog12.m6;

import be.kdg.prog12.m6.io.FileHandler;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * This class demonstrates reading and writing to/from text and binary files.
 * Exceptions are taken care of in this class.
 * A FileChooser is used as a file selection dialog.
 */
public class DemoMountainCabins extends Application {
    private static String DEFAULT_PATH = "3b_mountaincabins/resources/";

    @Override
    public void start(Stage primaryStage) {
        String file = selectSourceFile(primaryStage);
        List<MountainCabin> list = readSourceFile(file);
        showRecords(list, file);
        readNewRecord(list);
        file = selectDestinationFile(primaryStage);
        writeDestinationFile(list, file);
        Platform.exit();
    }

    private String selectSourceFile(Stage stage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(DEFAULT_PATH));
        fileChooser.setTitle("Choose source file");
        File file = fileChooser.showOpenDialog(stage);
        if ((file == null) || (file.getName().equals(""))) {
            System.out.println("Invalid source file");
            System.exit(1);
        }
        return file.getAbsolutePath();
    }

    private List<MountainCabin> readSourceFile(String file) {
        try {
            if (file.endsWith(".txt")) {
                return FileHandler.txtFile2List(file);
            } else if (file.endsWith(".bin")) {
                return FileHandler.binFile2List(file);
            } else {
                System.out.println("Invalid source file");
                System.exit(1);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    private void showRecords(List<MountainCabin> list, String file) {
        String title = "All records of " + file + " :";
        System.out.println(title);
        for (int i = 0; i < title.length(); i++) {
            System.out.print("-");
        }
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%2d) %s\n", i + 1, list.get(i));
        }
    }

    private void readNewRecord(List<MountainCabin> list) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nEnter the name of the new mountain cabin: ");
        String name = scanner.nextLine();
        System.out.print("Altitude: ");
        int altitude = scanner.nextInt();
        scanner.nextLine(); // flush keyboardbuffer
        System.out.print("Location: ");
        String location = scanner.nextLine();
        list.add(new MountainCabin(name, altitude, location));
        System.out.println("New record has been added.");
    }

    private String selectDestinationFile(Stage stage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(DEFAULT_PATH));
        fileChooser.setTitle("Choose destination file");
        File file = fileChooser.showSaveDialog(stage);
        if ((file == null) || (file.getName().equals(""))) {
            System.out.println("Invalid destination file");
            System.exit(1);
        }
        return file.getAbsolutePath();
    }

    private void writeDestinationFile(List<MountainCabin> list, String file) {
        try {
            if (file.endsWith(".txt")) {
                FileHandler.list2TxtFile(list, file);
                System.out.println("The data was saved as text");
            } else if (file.endsWith(".bin")) {
                FileHandler.list2BinFile(list, file);
                System.out.println("The data was saved in a binary format");
            } else {
                System.out.println("Invalid destination file, should be .txt or .bin");
                System.exit(1);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
