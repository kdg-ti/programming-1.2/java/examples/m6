package be.kdg.prog12.m6;

public class MountainCabin implements Comparable<MountainCabin> {
    private final String name;
    private final int altitude;
    private final String location;

    public MountainCabin(String name, int altitude, String location) {
        this.name = name;
        this.altitude = altitude;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public int getAltitude() {
        return altitude;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return String.format("%-20s (%4dm) located in %s", name, altitude, location);
    }

    @Override
    public int compareTo(MountainCabin o) {
        return this.name.compareTo(o.name);
    }
}
