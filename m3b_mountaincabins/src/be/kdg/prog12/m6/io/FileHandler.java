package be.kdg.prog12.m6.io;

import be.kdg.prog12.m6.MountainCabin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Contains 4 static methods to parse a binary file or a text file into a List and, vice versa, to
 * write a List's content to a binary file or a text file.
 */
public class FileHandler {
    public static List<MountainCabin> txtFile2List(String file) throws IOException {
        List<MountainCabin> list = new ArrayList<>();
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while ((line = br.readLine()) != null) {
                StringTokenizer tokenizer = new StringTokenizer(line, "#");
                String name = tokenizer.nextToken();
                int altitude = Integer.parseInt(tokenizer.nextToken());
                String location = tokenizer.nextToken();
                MountainCabin cabin = new MountainCabin(name, altitude, location);
                list.add(cabin);
            }
            Collections.sort(list);
            return list;
        } catch (NoSuchElementException | NumberFormatException e1) {
            throw new IOException("Reading error at line: " + line, e1);
        } catch (IOException e2) {
            throw new IOException("File " + file + " couldn't be read.", e2);
        }
    }

    public static List<MountainCabin> binFile2List(String file) throws IOException {
        List<MountainCabin> list = new ArrayList<>();
        try (DataInputStream is = new DataInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            try {
                while (true) {
                    String name = is.readUTF();
                    int altitude = is.readInt();
                    String location = is.readUTF();
                    MountainCabin cabin = new MountainCabin(name, altitude, location);
                    list.add(cabin);
                }
            } catch (EOFException e1) {
                // Everything OK; end of file reached.
            } catch (IOException e2) {
                throw new IOException("Error reading file " + file + ".", e2);
            }
            Collections.sort(list);
            return list;
        } catch (IOException e3) {
            throw new IOException("File " + file + " couldn't be read.", e3);
        }
    }

    public static void list2TxtFile(List<MountainCabin> list, String file) throws IOException {
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {
            for (MountainCabin cabin : list) {
                pw.printf("%s#%d#%s%n", cabin.getName(), cabin.getAltitude(), cabin.getLocation());
            }
        } catch (IOException e) {
            throw new IOException("Error while writing to file " + file + ".", e);
        }

    }

    public static void list2BinFile(List<MountainCabin> list, String file) throws IOException {
        try (DataOutputStream os = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            for (MountainCabin cabin : list) {
                os.writeUTF(cabin.getName());
                os.writeInt(cabin.getAltitude());
                os.writeUTF(cabin.getLocation());
            }
        } catch (IOException e) {
            throw new IOException("Error while writing to file " + file + ".", e);
        }
    }
}
