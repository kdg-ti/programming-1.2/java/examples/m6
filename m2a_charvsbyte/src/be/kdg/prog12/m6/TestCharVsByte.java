package be.kdg.prog12.m6;

import java.io.*;
import java.util.Formatter;
import java.util.Properties;

public class TestCharVsByte {
  public static void main(String[] args) {
    final String BASE_DIRECTORY = "FILES/TEST/";

    int a = 5;
    float b = 10.0f;
    boolean c = true;
    String d = "Hello World";

    try {
      Formatter formatter = new Formatter(BASE_DIRECTORY + "charfile.txt");
      DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(
        new FileOutputStream(BASE_DIRECTORY + "bytefile.bin")));

      formatter.format("%d%f%b%s", a, b, c, d);

      dos.writeInt(a);
      dos.writeFloat(b);
      dos.writeBoolean(c);
      dos.writeUTF(d);

      formatter.close();
      dos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    // A small extra: reading system properties
    Properties prop = System.getProperties();
    System.out.println("Directory: " + prop.getProperty("user.dir"));
    System.out.println("Home Directory: " + prop.getProperty("user.home"));
  }
}
