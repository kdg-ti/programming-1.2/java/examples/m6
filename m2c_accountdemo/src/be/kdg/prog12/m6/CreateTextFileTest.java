package be.kdg.prog12.m6;

import be.kdg.prog12.m6.io.TextFileWriter;

public class CreateTextFileTest {
    public static final String FILE_NAME = "m2c_accountdemo/resources/clients.txt";

    public static void main(String[] args) {
        TextFileWriter writer = new TextFileWriter();

        writer.openFile(FILE_NAME);
        writer.addRecords();
        writer.closeFile();
    }
}

/*
To terminate input, type 0 for the account number.

Enter account number (>0), first name, last name and balance
? 100 Bob Jones 24.98
Enter account number (>0), first name, last name and balance
? 200 Steve Doe -345.67
Enter account number (>0), first name, last name and balance
? 300 Pam White 0.00
Enter account number (>0), first name, last name and balance
? 400 Sam Stone -42.16
Enter account number (>0), first name, last name and balance
? 500 Sue Rich 224.62
Enter account number (>0), first name, last name and balance
? 0
*/
