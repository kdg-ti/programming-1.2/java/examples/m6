package be.kdg.prog12.m6;

import be.kdg.prog12.m6.io.TextFileReader;

public class ReadTextFileTest {
    public static final String FILE_NAME = "m2c_accountdemo/resources/clients.txt";

    public static void main(String[] args) {
        TextFileReader reader = new TextFileReader();
        reader.openFile(FILE_NAME);
        reader.readRecords();
        reader.closeFile();
    }
}

/*
Account   First Name  Last Name      Balance
100       Bob         Jones            24.98
200       Steve       Doe            -345.67
300       Pam         White             0.00
400       Sam         Stone           -42.16
500       Sue         Rich            224.62
*/
